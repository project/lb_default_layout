<?php

namespace Drupal\lb_default_layout\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Defines a metatag list class for better normalization targeting.
 */
class LayoutBuilderInformation extends FieldItemList {

  use ComputedItemListTrait;

  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $bundle = $this->getEntity()->bundle();

    // Loop through all layout and add default layout value.
    // layout_library prefix is optional. If omitted, all configuration object names that exist are returned.
    foreach (\Drupal::configFactory()->listAll("layout_library") as $config_item) {
      // We check if the current call has a suitable bundle from the layout library.
      // IE: [YOUR_URL]/jsonapi/node/landing_page would get the targetBundle landing_page
      if (strpos($bundle, \Drupal::configFactory()->get($config_item)->get('targetBundle')) !== false) {
        $this->list[0] = $this->createItem(0, \Drupal::configFactory()->get($config_item)->get('layout'));
      }
    }

  }
}
